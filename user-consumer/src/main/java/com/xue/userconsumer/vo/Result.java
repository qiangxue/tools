package com.xue.userconsumer.vo;

import com.xue.userconsumer.enums.ReturnEnum;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

/**
 * @Description: TODO
 * @Author: qiang_xue
 * @date: 2023/3/8 15:30
 * @package: com.xue.working.vo
 * @Version: 1.0
 */
@Getter
@Setter
@AllArgsConstructor
@Builder(toBuilder = true)
public class Result<T> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("标识代码,0表示成功，非0表示出错")
    private Integer code;
    @ApiModelProperty("提示信息,供报错时使用")
    private String message;
    @ApiModelProperty("返回的数据")
    private T data;

    public Result(Integer code, String message) {
        this.code = code;
        this.message = message;
    }
    public Result(T t) {
        this.data = t;
    }

    public static <T> Result<T> ok() {
        return new Result<>(ReturnEnum.SUCCESS.code(), ReturnEnum.SUCCESS.message(), null);
    }
    public static <T> Result<T> data(T data) {
        return new Result<>(ReturnEnum.SUCCESS.code(), ReturnEnum.SUCCESS.message(), data);
    }

    public static <T> Result<T> error() {
        return new Result<>(ReturnEnum.FAILURE.code(), ReturnEnum.FAILURE.message(), null);
    }
    public static <T> Result<T> error(String msg) {
        return new Result<>(ReturnEnum.FAILURE.code(), msg, null);
    }

    public static <T> Result<T> get(int code, String msg, T data) {
        return new Result<>(code, msg, data);
    }
}
