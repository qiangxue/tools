package com.xue.userconsumer.vo;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author qiang
 * @since 2023-03-08
 */
@Data
@ApiModel(value="AutoMail对象", description="对象")
public class AutoMail implements Serializable {

    private static final long serialVersionUID = 1L;

    private String mailserverhost;

    private String mailserverport;

    private String validatemain;

    private String username;

    private String password;

    private String fromaddress;

    private String subject;

    private String content;


}
