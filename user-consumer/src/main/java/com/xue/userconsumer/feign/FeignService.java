package com.xue.userconsumer.feign;

import com.xue.userconsumer.vo.AutoMail;
import com.xue.userconsumer.vo.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * @Description: TODO
 * @Author: qiang_xue
 * @date: 2023/3/23 14:00
 * @package: com.xue.userconsumer.feign
 * @Version: 1.0
 */
@FeignClient(value="user-provider")
public interface FeignService {

    @RequestMapping(value="/searchByName")
    Result<List<AutoMail>> searchByName();
}
