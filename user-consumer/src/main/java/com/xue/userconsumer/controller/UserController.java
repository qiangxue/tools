package com.xue.userconsumer.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.List;

/**
 * @Description: TODO
 * @Author: qiang_xue
 * @date: 2023/3/10 16:43
 * @package: com.xue.userconsumer.controller
 * @Version: 1.0
 */
@Api(tags = "UserConsumer")
@AllArgsConstructor
@RestController
@RequestMapping("/consumer")
public class UserController {

    private final RestTemplate restTemplate;

    private final DiscoveryClient discoveryClient;

    @ApiOperation(value = "查询用户")
    @RequestMapping("/{id}")
    public Object findById(@PathVariable Integer id) {
        System.out.println("id = " + id);

        List<ServiceInstance> serviceInstances = discoveryClient.getInstances("user-provider");
        ServiceInstance serviceInstance = serviceInstances.get(0);

        String url = "http://" + serviceInstance.getHost() + ":" + serviceInstance.getPort() + "/user/find/" + id;
        System.out.println(url);

        //"http://localhost:8762/user/find/"+id
        //https://blog.csdn.net/weixin_48235405/article/details/122335996
        String result = restTemplate.getForObject(url, String.class);
        System.out.println(result);

        return result;
    }
}
