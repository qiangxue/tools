package com.xue.userconsumer.controller;

import com.xue.userconsumer.feign.FeignService;
import com.xue.userconsumer.vo.AutoMail;
import com.xue.userconsumer.vo.Result;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @Description: TODO
 * @Author: qiang_xue
 * @date: 2023/3/23 14:07
 * @package: com.xue.userconsumer.controller
 * @Version: 1.0
 */
@RestController
@RequestMapping("/user")
@AllArgsConstructor
public class MailConfigController {
    private final FeignService feignService;

    @PostMapping ("/searchByName")
    public Result<List<AutoMail>> searchByName(@RequestParam(value = "name") String name){

        return feignService.searchByName();
    }
}
