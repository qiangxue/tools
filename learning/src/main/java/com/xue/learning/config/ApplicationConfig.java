package com.xue.learning.config;

import com.xue.learning.entity.ValueBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author qiang_xue
 */
@Configuration
public class ApplicationConfig {
    @Bean
    public ValueBean valueBean(){

        ValueBean valueBean = new ValueBean();
        valueBean.setValue1("1");
        return valueBean;
    }

}
