package com.xue.learning.functional.vo;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author qiang_xue
 */
@Getter
@Setter
@NoArgsConstructor
public class BasisVo {
    private int intField;
    private String strField;
    private Object objField;
}
