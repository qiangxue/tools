package com.xue.learning.functional.vo;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author qiang_xue
 */
@Getter
@Setter
@NoArgsConstructor
public class OriginalVo {

    private int age;
    private String name;
}
