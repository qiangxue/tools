package com.xue.learning.example;

import org.junit.Test;

import java.util.StringJoiner;

/**
 * @author qiang_xue
 */
public class StringJionerExampleTest {

    @Test
    public void test(){
        StringJoiner sj = new StringJoiner(":", "[", "]");
        sj.add("George").add("Sally").add("Fred");
        String desiredString = sj.toString();
        System.out.println(desiredString);
        /*
        [George:Sally:Fred]
         */
    }
}
