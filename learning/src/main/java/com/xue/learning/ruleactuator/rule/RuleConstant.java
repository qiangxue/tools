package com.xue.learning.ruleactuator.rule;

/**
 * 常量定义
 * @author qiang_xue
 */
public class RuleConstant {
    public static final String MATCH_ADDRESS_START = "北京";
    public static final String MATCH_NATIONALITY_START = "中国";
}
