package com.xue.learning.ruleactuator;

import com.xue.learning.ruleactuator.rule.RuleDto;

// 规则抽象
public interface BaseRule {
    boolean execute(RuleDto dto);
}
