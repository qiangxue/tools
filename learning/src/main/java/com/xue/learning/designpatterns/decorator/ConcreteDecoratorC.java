package com.xue.learning.designpatterns.decorator;

/**
 * 具体装饰类C
 * @author qiang_xue
 */
public class ConcreteDecoratorC extends Decorator{

    /**
     * 被装饰者
     */
    Component component;

    public ConcreteDecoratorC(Component component){
        this.component = component;
    }

    @Override
    public String getDescription() {
        return component.getDescription() + ",decoratorCContext";
    }
}
