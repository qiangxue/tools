package com.xue.learning.designpatterns.decorator;

/**
 * 组件抽象类
 * @author qiang_xue
 */
public abstract class Component {

    String description = "Component";

    public String getDescription() {
        return description;
    }
}
