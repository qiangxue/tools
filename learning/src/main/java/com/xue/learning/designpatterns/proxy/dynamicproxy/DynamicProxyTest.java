package com.xue.learning.designpatterns.proxy.dynamicproxy;

import com.xue.learning.designpatterns.proxy.TargetService;
import com.xue.learning.designpatterns.proxy.TargetServiceImpl;
import org.junit.Test;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;

/**
 * @author qiang_xue
 */
public class DynamicProxyTest {

    @Test
    public void test(){
        TargetService targetServiceImpl = new TargetServiceImpl();
        InvocationHandler handler = new TargetServiceInvocationHandler<>(targetServiceImpl);
        TargetService targetServiceImplProxy = (TargetService) Proxy.newProxyInstance(TargetService.class.getClassLoader(),new Class[]{TargetService.class},handler);
        targetServiceImplProxy.nothing("1");
    }
}
