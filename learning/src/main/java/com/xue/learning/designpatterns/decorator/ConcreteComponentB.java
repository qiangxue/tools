package com.xue.learning.designpatterns.decorator;

/**
 * 具体组件类B
 * @author qiang_xue
 */
public class ConcreteComponentB extends Component {

    public ConcreteComponentB() {
        description = "concreteComponentB";
    }
}
