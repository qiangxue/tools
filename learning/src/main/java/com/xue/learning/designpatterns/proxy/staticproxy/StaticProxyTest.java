package com.xue.learning.designpatterns.proxy.staticproxy;

import com.xue.learning.designpatterns.proxy.TargetService;
import com.xue.learning.designpatterns.proxy.TargetServiceImpl;
import org.junit.Test;

/**
 * @author qiang_xue
 */
public class StaticProxyTest {

    @Test
    public void test(){

        TargetService targetService = new TargetServiceImpl();
        TargetServiceProxy proxy = new TargetServiceProxy(targetService);
        proxy.doSomething();
    }
}
