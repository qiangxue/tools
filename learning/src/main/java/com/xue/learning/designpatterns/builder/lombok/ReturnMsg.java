package com.xue.learning.designpatterns.builder.lombok;

/**
 * @author qiang_xue
 * @date 2019/6/20 11:42
 */
public class ReturnMsg {

    public static final String SUCC_CODE = "0";
    public static final String SUCCESS = "成功";

}
