package com.xue.learning.designpatterns.proxy.staticproxy;

import com.xue.learning.designpatterns.proxy.TargetService;
import com.xue.learning.designpatterns.proxy.TargetServiceImpl;

/**
 * @author qiang_xue
 */
public class TargetServiceProxy implements TargetService {

    private TargetService targetService;

    public TargetServiceProxy(TargetService targetService){
        if(targetService.getClass() == TargetServiceImpl.class){
            this.targetService = targetService;
        }
    }

    @Override
    public void doSomething() {

        System.out.println("Before DoSomething...");
        targetService.doSomething();
        System.out.println("After DoSomething...");

    }

    @Override
    public void nothing(String a) {

    }
}
