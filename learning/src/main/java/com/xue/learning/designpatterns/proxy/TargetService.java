package com.xue.learning.designpatterns.proxy;

/**
 * @author qiang_xue
 * @Description:服务类接口
 * @Date: 10:12 2021/4/20
 */
public interface TargetService {
    void doSomething();

    void nothing(String a);
}
