package com.xue.learning.designpatterns.decorator;

/**
 * 具体组件类A
 * @author qiang_xue
 */
public class ConcreteComponentA extends Component {

    public ConcreteComponentA() {
        description = "concreteComponentA";
    }
}
