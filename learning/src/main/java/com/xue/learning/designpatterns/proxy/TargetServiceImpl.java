package com.xue.learning.designpatterns.proxy;

/**
 * @author qiang_xue
 */
public class TargetServiceImpl implements TargetService {

    @Override
    public void doSomething() {
        System.out.println("do an important thing!");
    }

    @Override
    public void nothing(String a) {
        System.out.println("done nothing!");
    }
}
