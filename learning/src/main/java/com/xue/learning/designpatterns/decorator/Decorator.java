package com.xue.learning.designpatterns.decorator;

/**
 * 装饰抽象类
 */
public abstract class Decorator extends Component {

    @Override
    public abstract String getDescription();

}
