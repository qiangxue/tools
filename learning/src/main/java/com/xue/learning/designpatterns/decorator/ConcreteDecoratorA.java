package com.xue.learning.designpatterns.decorator;

/**
 * 具体装饰类A
 * @author qiang_xue
 */
public class ConcreteDecoratorA extends Decorator{

    /**
     * 被装饰者
     */
    Component component;

    public ConcreteDecoratorA(Component component){
        this.component = component;
    }

    @Override
    public String getDescription() {
        return component.getDescription() + ",decoratorAContext";
    }
}
