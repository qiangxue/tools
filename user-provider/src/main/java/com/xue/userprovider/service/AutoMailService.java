package com.xue.userprovider.service;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xue.userprovider.entity.AutoMail;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author qiang
 * @since 2023-03-08
 */
public interface AutoMailService extends IService<AutoMail> {

    int add(AutoMail autoMail);

    int update(AutoMail autoMail);

    Page<AutoMail> searchPage(Page<AutoMail> page);

    List<AutoMail> searchByName(String name);

}
