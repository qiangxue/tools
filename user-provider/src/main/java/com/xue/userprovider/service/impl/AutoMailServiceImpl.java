package com.xue.userprovider.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xue.userprovider.entity.AutoMail;
import com.xue.userprovider.mapper.AutoMailMapper;
import com.xue.userprovider.service.AutoMailService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author qiang
 * @since 2023-03-08
 */
@Service
@Slf4j
public class AutoMailServiceImpl extends ServiceImpl<AutoMailMapper, AutoMail> implements AutoMailService {

    @Resource
    private AutoMailMapper autoMailMapper;
    @Override
    public int add(AutoMail autoMail) {
        return autoMailMapper.insert(autoMail);
    }

    @Override
    public int update(AutoMail autoMail) {
        return autoMailMapper.updateById(autoMail);
    }

    @Override
    public Page<AutoMail> searchPage(Page<AutoMail> page) {
        return autoMailMapper.selectPage(page, null);
    }

    @Override
    public List<AutoMail> searchByName(String name) {
        return autoMailMapper.searchByName(name);
    }
}
