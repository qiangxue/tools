package com.xue.userprovider.enums;

/**
 * @Description: TODO
 * @Author: qiang_xue
 * @date: 2023/3/8 15:39
 * @package: com.xue.working.defenum
 * @Version: 1.0
 */
public enum ReturnEnum {

    SUCCESS(0, "成功"),
    FAILURE(1, "失败");

    private final Integer code;
    private final String message;

    ReturnEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public Integer code() {
        return code;
    }

    public String message() {
        return message;
    }
}
