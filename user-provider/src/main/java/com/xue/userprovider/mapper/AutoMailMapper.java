package com.xue.userprovider.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xue.userprovider.entity.AutoMail;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author qiang
 * @since 2023-03-08
 */
@Mapper
public interface AutoMailMapper extends BaseMapper<AutoMail> {

    List<AutoMail> searchByName(String name);
}
