package com.xue.userprovider.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xue.userprovider.entity.AutoMail;
import com.xue.userprovider.enums.ReturnEnum;
import com.xue.userprovider.service.AutoMailService;
import com.xue.userprovider.vo.PageVo;
import com.xue.userprovider.vo.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 *
 * @author qiang
 * @since 2023-03-08
 */
@Slf4j
@Api(tags = "获取邮件配置")
@RestController
@RequestMapping("/auto-mail")
public class AutoMailController {

    @Resource
    AutoMailService autoMailService;

    @ApiOperation("增加")
    @PostMapping("/add")
    public Result<AutoMail> add(@RequestBody AutoMail autoMail){
        log.info("user: {}", autoMail);
        int insert = autoMailService.add(autoMail);
        if (insert > 0){
            log.info("插入成功！");
            return new Result<>(ReturnEnum.SUCCESS.code(), ReturnEnum.SUCCESS.message());
        }
        log.error("插入失败！");
        return new Result<>(ReturnEnum.FAILURE.code(), ReturnEnum.FAILURE.message());

    }

    @ApiOperation("更新")
    @PostMapping("/update")
    public Result<AutoMail> update(@RequestBody AutoMail autoMail){
        log.info("user: {}", autoMail);
        int update = autoMailService.update(autoMail);
        if (update > 0){
            log.info("更新成功！");
            return new Result<>(ReturnEnum.SUCCESS.code(), ReturnEnum.SUCCESS.message());
        }
        log.error("更新失败！");
        return new Result<>(ReturnEnum.FAILURE.code(), ReturnEnum.FAILURE.message());

    }

    @ApiOperation("查询")
    @PostMapping("/searchByName")
    public Result<List<AutoMail>> searchByName(@RequestParam(value = "name") String name){
        List<AutoMail> list = autoMailService.searchByName(name);
        return new Result<>(list);

    }
}
