package com.xue.userprovider.config;

import io.swagger.annotations.ApiOperation;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import springfox.documentation.builders.*;
import springfox.documentation.oas.annotations.EnableOpenApi;
import springfox.documentation.schema.ScalarType;
import springfox.documentation.service.*;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

import java.util.ArrayList;
import java.util.List;

@Configuration
@EnableOpenApi
public class SwaggerConfig {

    @Value("${swagger.enabled}")
    private boolean enable;
    @Bean
    @SneakyThrows
    public Docket createApi() {
        return new Docket(DocumentationType.OAS_30)
                .apiInfo(apiInfo())
                .enable(enable)
                .globalRequestParameters(null)
                // 设置自定义返回消息体
                .globalResponses(HttpMethod.GET, globalResponse())
                .globalResponses(HttpMethod.POST, globalResponse())
                .select()
                .apis(RequestHandlerSelectors.withMethodAnnotation(ApiOperation.class))
                .paths(PathSelectors.any())
                .build()
                .groupName("");
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("User服务Swagger3接口文档")
                .description("v 1.0")
                .contact(new Contact("Qiang", "xxxxx", "xxx"))
                .version("1.0.0")
                .build();
    }




    /**
     * 功能描述: globalResponse
     **/
    private List<Response> globalResponse() {
        List<Response> responseList = new ArrayList<>();
        responseList.add(new ResponseBuilder().code("400").description("错误请求").build());
        responseList.add(new ResponseBuilder().code("401").description("未认证").build());
        responseList.add(new ResponseBuilder().code("403").description("请求被禁止").build());
        responseList.add(new ResponseBuilder().code("404").description("找不到资源").build());
        responseList.add(new ResponseBuilder().code("500").description("服务器内部错误").build());
        return responseList;
    }
}