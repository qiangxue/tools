package com.xue.userprovider.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author qiang
 * @since 2023-03-08
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("AUTO_MAIL")
@ApiModel(value="AutoMail对象", description="")
public class AutoMail implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableField("MAILSERVERHOST")
    private String mailserverhost;

    @TableField("MAILSERVERPORT")
    private String mailserverport;

    @TableField("VALIDATEMAIN")
    private String validatemain;

    @TableField("USERNAME")
    private String username;

    @TableField("PASSWORD")
    private String password;

    @TableField("FROMADDRESS")
    private String fromaddress;

    @TableField("SUBJECT")
    private String subject;

    @TableField("CONTENT")
    private String content;


}
