package com.pkufi.work.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
//        .csrf().disable() // 关闭CSRF保护
        .authorizeRequests()
            .antMatchers("/login", "/css/**", "/js/**", "/icons/**").permitAll() // 允许所有人访问登录页面和静态资源
            .antMatchers("/compareExcelToPage").permitAll()
            .antMatchers("/fin/**").permitAll()
            .antMatchers("/fee/**").permitAll()
            .antMatchers("/**").permitAll()
                .anyRequest().authenticated() // 其他请求需要认证
            .and()
        .formLogin()
            .loginPage("/login") // 登录页面
            .failureUrl("/login?error=true") // 登录失败后跳转的页面
            .defaultSuccessUrl("/index", true) // 登录成功后跳转的页面
            .permitAll()
            .and()
        .logout()
            .permitAll();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
