package com.pkufi.work.listener;

import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;


@Component
public class StartupListener implements ApplicationListener<ApplicationReadyEvent> {

    private boolean isAppStarted = false;

    @Override
    public void onApplicationEvent(ApplicationReadyEvent event) {
        if (!isAppStarted) {
            // 在应用启动时执行一些初始化逻辑
            // 或者判断定时任务是否需要执行，设置相关标志位等
            // 例如，您可以在此处设置一个标志位，表示应用已经启动完成
            // 或者执行定时任务的条件判断，并设置相关属性来决定定时任务是否执行
            // ...
            isAppStarted = true;
        }
    }
}

