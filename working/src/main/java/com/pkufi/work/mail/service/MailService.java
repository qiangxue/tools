package com.pkufi.work.mail.service;

import com.pkufi.work.mail.vo.Mail;
import java.util.List;

public interface MailService {
    List<Mail> getMails(String subjectToSearch);
}
