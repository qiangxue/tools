package com.pkufi.work.mail.controller;

import com.pkufi.work.mail.service.MailService;
import com.pkufi.work.mail.vo.Mail;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@AllArgsConstructor
public class MailController {

    private final MailService mailService;

    @GetMapping("/mails")
    public List<Mail> getMails(@RequestParam String subject) {
        return mailService.getMails(subject);
    }
}