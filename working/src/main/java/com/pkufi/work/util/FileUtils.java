package com.pkufi.work.util;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * @Description: TODO
 * @Author: qiang_xue
 * @date: 2023/3/7 10:35
 * @package: com.xue.working.util
 * @Version: 1.0
 */
@Component
@Slf4j
@AllArgsConstructor
public class FileUtils {

    private final ResourceLoader resourceLoader;

    /**
     * 读取脚本
     */
    public String readTemplate(String templatePath) {

        StringBuilder sb = new StringBuilder();
        //try-with-resources(省略关闭资源)
        try (InputStream is = resourceLoader.getResource(templatePath).getInputStream();
             BufferedReader br = new BufferedReader(new InputStreamReader(is))) {
            String dataLine;
            while ((dataLine = br.readLine()) != null) {
                sb.append(dataLine);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        log.info("Template loaded: {}", templatePath);
        return sb.toString();
    }

    public void generateScript(String data, String targetFile) {

        data = data.replaceAll(";", ";\r\n");

        try (InputStream is = new ByteArrayInputStream(data.getBytes());
             OutputStream os = Files.newOutputStream(Paths.get(targetFile))) {
            byte[] buffer = new byte[1024];
            int bytesRead;
            while ((bytesRead = is.read(buffer)) != -1) {
                os.write(buffer, 0, bytesRead);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
