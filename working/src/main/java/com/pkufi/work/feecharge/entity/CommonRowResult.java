package com.pkufi.work.feecharge.entity;

import java.util.HashMap;
import java.util.Map;

public class CommonRowResult {
    private Map<String, Object> rowMap;

    public CommonRowResult() {
        this.rowMap = new HashMap<>();
    }

    public void put(String key, Object value) {
        this.rowMap.put(key, value);
    }

    public Object get(String key) {
        return this.rowMap.get(key);
    }

    public Map<String, Object> getRowMap() {
        return rowMap;
    }

    @Override
    public String toString() {
        return "CommonRowResult{" +
                "rowMap=" + rowMap +
                '}';
    }
}