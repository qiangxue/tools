package com.pkufi.work.feecharge.repository.impl;

import com.pkufi.work.feecharge.entity.CommonRowResult;
import com.pkufi.work.feecharge.repository.RefundDataRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
public class RefundDataRepositoryImpl implements RefundDataRepository {

    private final JdbcTemplate primaryJdbcTemplate;

    private final JdbcTemplate secondaryJdbcTemplate;

    public RefundDataRepositoryImpl(@Qualifier("primaryJdbcTemplate") JdbcTemplate primaryJdbcTemplate, @Qualifier("secondaryJdbcTemplate") JdbcTemplate secondaryJdbcTemplate) {
        this.primaryJdbcTemplate = primaryJdbcTemplate;
        this.secondaryJdbcTemplate = secondaryJdbcTemplate;
    }

    @Override
    public List<CommonRowResult> queryRefundData(Object... params){

        String sql = "select cc.batchseqid,t.detail_id, " +
                "      cc.return_state || '-' || decode(cc.return_state, " +
                "                                        'W', " +
                "                                        '待报盘', " +
                "                                        'N', " +
                "                                        '已报盘待回盘', " +
                "                                        'Y', " +
                "                                        '已回盘', " +
                "                                        'P', " +
                "                                        '已回盘') 报回盘状态, " +
                "      decode(aa.pay_state, 1, '未支付', 2, '已支付', 3, '支付失败', ' ** ') 收付状态, " +
                "      (select bbb.sales_channel_name " +
                "          from t_sales_channel bbb, t_contract_master cm " +
                "        where bbb.sales_channel_id = cm.channel_type " +
                "          and cm.policy_id = t.policy_id) sales_channel, " +
                "      t.reference policy_code, " +
                "      t.collect_pay || '-' || " +
                "      decode(t.collect_pay, 1, 'Collection', 2, 'Payment') collect_pay, " +
                "      t.pay_mode || '-' || " +
                "      pkg_pub_multi_lang.F_GET_CODE_DESC('T_PAY_MODE', t.pay_mode, 211) pay_mode, " +
                "      decode(aa.attribute3 ,0,'对公',1,'对私') 对公对私, " +
                "      t.fee_amount, " +
                "      t.text_status || '-' || " +
                "      pkg_pub_multi_lang.F_GET_CODE_DESC('T_BANK_TEXT_STATUS', " +
                "                                          t.text_status, " +
                "                                          211) transfer_status, " +
                "      t.bank_code || '-' || " +
                "      (select a.bank_name from t_bank a where a.bank_code = t.bank_code) 收付通道, " +
                "      t.acco_name, " +
                "      t.real_bank_code || '-' || " +
                "      (select b.bank_name " +
                "          from t_bank b " +
                "        where b.bank_code = t.real_bank_code) 银行, " +
                "      t.account, " +
                "      aa.paydate, " +
                "      (select pc.service_id || '-' || " +
                "              pkg_pub_multi_lang.F_GET_CODE_DESC('T_SERVICE', " +
                "                                                  pc.service_id, " +
                "                                                  211) " +
                "          from t_policy_change pc " +
                "        where pc.master_chg_id = t.master_chg_id " +
                "          and rownum = 1) 变更项,aa.BZTYPE " +
                ",decode(aa.BZTYPE,'CAP0UM','代收','CAP0RS','代收','CAP0RM','代收','CAP0PM','代收','CAP0NS','代收','CAP0NM','代收','代付') BZTYPE " +
                "  from t_bank_text_detail t, t_capital_platform aa, t_capital_switch cc " +
                "  where t.detail_id = aa.detailseqid(+) " +
                "  and aa.batchseqid = cc.batchseqid " +
                "  and t.collect_pay = 2 " +
                "  and cc.return_state in ('H','W','N','Y') " +
                "  and aa.pay_state<>9 " +
                "  and aa.paydate between ? and ?";

        return primaryJdbcTemplate.query(sql, params, new RowMapper<CommonRowResult>() {
            @Override
            public CommonRowResult mapRow(ResultSet rs, int rowNum) throws SQLException {
                CommonRowResult commonRowResult = new CommonRowResult();
                int columnCount = rs.getMetaData().getColumnCount();
                for (int i = 1; i <= columnCount; i++) {
                    String columnName = rs.getMetaData().getColumnName(i);
                    Object value = rs.getObject(i);
                    commonRowResult.put(columnName, value);
                }
                return commonRowResult;
            }
        });
    }
}