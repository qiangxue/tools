package com.pkufi.work.feecharge.controller;

import com.pkufi.work.feecharge.service.FeesAndChargesService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;


/**
 * @Description: TODO
 * @Author: qiang_xue
 * @date: 2023/3/6 14:59
 * @package: com.xue.working.controller
 * @Version: 1.0
 */
@RestController
@AllArgsConstructor
@Api(value = "收费付费", tags = "FeesAndCharges")
@RequestMapping("/api/feesAndCharges")
public class FeesAndChargesController {

    private final FeesAndChargesService feesAndChargesService;

    @ApiOperation(value = "加扣")
    @PostMapping("/addFees")
    public String addPaymentToText(@RequestParam(value = "policyNo") String policyNo,
                                   @RequestParam(value = "amount") String amount) throws IOException {

        feesAndChargesService.executeAddFees(policyNo, amount);

        return "succ addFees";
    }


    @ApiOperation(value = "批量加扣")
    @GetMapping("/batchAddFees")
    public String batchAddPaymentToText() throws Exception {
        feesAndChargesService.executeBatchAddFees();
        return "succ batchAddFees";
    }
}
