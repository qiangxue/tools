package com.pkufi.work.feecharge.service.impl;

import com.pkufi.work.feecharge.service.TestEmailService;
import com.pkufi.work.mail.service.SendEmailHandler;
import com.pkufi.work.mail.vo.EmailData;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @Description: TODO
 * @Author qiang_xue
 * @Date 2022/8/1 16:13
 * @Version 1.0
 */
@Service
@AllArgsConstructor
public class TestEmailServiceImpl implements TestEmailService {

    private final static String subject = "付费数据";
    private final static String attachmentName = "file:///c:/";
    private final static String attachmentPath = "付费数据.xlsx";

    private final SendEmailHandler sendEmailHandler;

    @Override
    public void testEmail() {

        Date date = new Date();
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
        List<String> toList = new ArrayList<>();
        toList.add("shuanglu@pkufi.com");
        List<String> ccList = new ArrayList<>();
        EmailData emailData = EmailData.builder()
                .toList(toList)
                .ccList(ccList)
                .subject(format.format(date)+subject)
                .context("")
                .attachmentName(new ArrayList<>() {{
                    this.add(format.format(date) + attachmentName);
                }})
                .attachmentPath(attachmentPath)
                .build();

        sendEmailHandler.sendEMail(emailData,false);
    }
}
