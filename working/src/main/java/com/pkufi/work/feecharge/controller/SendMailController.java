package com.pkufi.work.feecharge.controller;

import com.pkufi.work.feecharge.service.TestEmailService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.mail.MessagingException;

/**
 * @Description: TODO
 * @Author qiang_xue
 * @Date 2022/7/27 16:26
 * @Version 1.0
 */
@RestController
@AllArgsConstructor
@Api(tags = "TestEmail")
@RequestMapping("/mail")
public class SendMailController {

    private final TestEmailService testEmailService;

    @ApiOperation(value = "发送邮件")
    @GetMapping("/send")
    public String send() throws MessagingException {

        //testEmailService.testEmail();
        return "发送";
    }
}
