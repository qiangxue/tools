package com.pkufi.work.feecharge.service;

import java.io.IOException;

/**
 * @Description: TODO
 * @Author: qiang_xue
 * @date: 2023/3/7 10:30
 * @package: com.xue.working.service
 * @Version: 1.0
 */
public interface FeesAndChargesService {

    void executeAddFees(String policyNo, String amount) throws IOException;

    void executeBatchAddFees() throws Exception;

    boolean executeBatchAddFees(String filePath) throws Exception;
}
