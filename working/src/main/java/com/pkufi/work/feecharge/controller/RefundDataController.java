package com.pkufi.work.feecharge.controller;

import com.pkufi.work.feecharge.entity.CommonRowResult;
import com.pkufi.work.feecharge.service.RefundDataService;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
@Controller
public class RefundDataController {

    private static final Logger log = LoggerFactory.getLogger(RefundDataController.class);
    private final RefundDataService refundDataService;

    @GetMapping("/refundDataPage")
    public String showRefundDataPage() {
        return "fee/refundDataPage";
    }

    @GetMapping("/queryRefundDataByDate")
    @ResponseBody
    public PageResult<CommonRowResult> queryRefundDataByDate(
            @RequestParam("startDate") String startDate,
            @RequestParam("endDate") String endDate,
            @RequestParam("page") int page,
            @RequestParam("size") int size) {

        List<CommonRowResult> results = refundDataService.getRefundDataByDate(startDate, endDate);

        int total = results.size();
        List<CommonRowResult> pagedResults = results.stream()
                .skip(page * size)
                .limit(size)
                .collect(Collectors.toList());
        log.info("query endDate:{} page:{} size:{}", endDate, page, size);
        return new PageResult<>(pagedResults, total);
    }

    public static class PageResult<T> {
        private List<T> content;
        private int total;

        public PageResult(List<T> content, int total) {
            this.content = content;
            this.total = total;
        }

        public List<T> getContent() {
            return content;
        }

        public int getTotal() {
            return total;
        }
    }

    @GetMapping("/export")
    @ResponseBody
    public String exportAndSendEmail(
            @RequestParam("startDate") String startDate,
            @RequestParam("endDate") String endDate) {

        refundDataService.exportAndSendEmail(startDate, endDate);

        return "success";
    }
}