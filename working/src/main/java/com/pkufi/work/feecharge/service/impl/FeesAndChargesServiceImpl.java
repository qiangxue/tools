package com.pkufi.work.feecharge.service.impl;

import com.pkufi.work.feecharge.service.FeesAndChargesService;
import com.pkufi.work.mail.service.SendEmailHandler;
import com.pkufi.work.mail.vo.EmailData;
import com.pkufi.work.util.FileUtils;
import com.pkufi.work.util.GenerateSqlWithExcel;
import com.pkufi.work.util.UniversalTools;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * @Description: TODO
 * @Author: qiang_xue
 * @date: 2023/3/7 10:30
 * @package: com.xue.working.service.impl
 * @Version: 1.0
 */
@Service
@Slf4j
@RequiredArgsConstructor
public class FeesAndChargesServiceImpl implements FeesAndChargesService {

    private final FileUtils fileUtils;
    private final GenerateSqlWithExcel generateSqlWithExcel;
    private final SendEmailHandler sendEmailHandler;
    private final ResourceLoader resourceLoader;
    private static final String templatePath = "classpath:addfeessql/template.sql";

    private static final String excelPathWin = "D:\\OneDrive\\win桌面\\脚本\\收付费\\批量加扣";
    private static final String targetBatchAddFeesWin = "D:\\OneDrive\\win桌面\\脚本\\收付费\\批量加扣\\脚本";
    private static final String excelPath = "/Users/xueqiang/Library/CloudStorage/OneDrive-个人/win桌面/脚本/收付费/批量加扣";
    private static final String targetBatchAddFees = "/Users/xueqiang/Library/CloudStorage/OneDrive-个人/win桌面/脚本/收付费/批量加扣/脚本";

    private static final String subject = "加扣脚本执行";

    @Value("${fromMail.fromAddr}")
    private String fromAddr;
    @Override
    public void executeAddFees(String policyNo, String amount) throws IOException {
        //读取模板
        log.info("读取模板脚本！");
        String data = fileUtils.readTemplate(templatePath);
        //替换内容
        /*
        %inputParameter_0%
        %inputParameter_1%
         */
        log.info("赋值脚本！");
        data = data.replace("%inputParameter_0%", policyNo).replace("%inputParameter_1%", amount);
        //生成脚本
        log.info("生成脚本！");
        //ClassPathResource resource = new ClassPathResource("classpath:addfeessql/");
        Resource resource = resourceLoader.getResource("classpath:addfeessql");

        String targetFilePath = resource.getFile()
                .getAbsolutePath()
                .replace("target\\classes", "src\\main\\resources")
                .replace("classes", "main/resources");

        String targetFileName = policyNo+"加扣脚本.sql";

        fileUtils.generateScript(data,targetFilePath+"\\"+targetFileName);
        //发送邮件
        log.info("发送执行脚本！");
        EmailData ed = EmailData.builder()
                .fromAddr(fromAddr)
                .toList(new ArrayList<>() {{
                    this.add("shuanglu@pkufi.com");
                }})
//                .ccList(new ArrayList<String>(){{
//                    this.add("frank_zhang@pkufi.com");
//                    this.add("kevin_li@pkufi.com");
//                    this.add("weiming_wu@pkufi.com");}})
//                    this.add("qiang_xue@pkufi.com");}})
                .ccList(new ArrayList<>() {{
                    this.add("qiang_xue@pkufi.com");
                }})
                .subject(subject)
                .context("Dear\r\n" +
                        "    请帮忙执行附件脚本，谢谢。\r\n")
                .attachmentPath(targetFilePath)
                .attachmentName(new ArrayList<>() {{
                    this.add(targetFileName);
                }})
                .build();
        sendEmailHandler.sendEMail(ed,true);

    }

    /**
     * 解析excel数据生成脚本
     * @throws Exception
     */
    @Override
    public void executeBatchAddFees() {

        Calendar c = Calendar.getInstance();
        int month = c.get(Calendar.MONTH)+1;
        int date = c.get(Calendar.DATE);
        String filePath;
        if(UniversalTools.isWin()){
            filePath = excelPathWin + "\\" + month + "." + date + "加扣明细.xlsx";
        }else{
            filePath = excelPath + "/" + month + "." + date + "加扣明细.xlsx";
        }

        executeBatchAddFees(filePath);
    }

    //重载
    @Override
    public boolean executeBatchAddFees(String filePath) {

        //读取模板
        log.info("读取模板脚本！");
        String data;
        data = generateSqlWithExcel.readExcelToSql(filePath);

        if((data == null) || data.isEmpty()){
            throw new RuntimeException("没有脚本内容！");
        }
        //生成脚本
        //Calendar c = Calendar.getInstance();
        log.info("生成脚本！");
        SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");
        String now = df.format(System.currentTimeMillis());
        String targetFileName = "1-批量加扣"+now+".sql";
        if(UniversalTools.isWin()){
            //windows系统路径
            fileUtils.generateScript(data,targetBatchAddFeesWin+"\\"+targetFileName);
        }else{
            //mac系统路径
            fileUtils.generateScript(data,targetBatchAddFees+"/"+targetFileName);
        }
        //发送邮件
        log.info("发送执行脚本！");
        EmailData ed = EmailData.builder()
                .fromAddr(fromAddr)
                .toList(new ArrayList<>() {{
                    this.add("jayne_zhang@pkufi.com");
//                    this.add("qiang_xue@pkufi.com");
                }})
//                .ccList(new ArrayList<String>(){{
//                    this.add("qiang_xue@pkufi.com");}})
//                .toList(new ArrayList<String>(){{
//                    this.add("qiang_xue@pkufi.com");}})
                .ccList(new ArrayList<>() {{
                    this.add("frank_zhang@pkufi.com");
                    this.add("kevin_li@pkufi.com");
                    this.add("yuting_huang@pkufi.com");
                    // this.add("weiming_wu@pkufi.com");
                }})
                .bccList(new ArrayList<>() {{
                    this.add("qiang_xue@pkufi.com");
                }})
                .subject(subject)
                .context("<html>" +
                        "    <head>" +
                        "        <title>加扣</title>" +
                        "    </head>" +
                        "    <body>" +
                        "        Dear <br/>" +
                        "        &nbsp&nbsp&nbsp&nbsp请帮忙按1、2、3顺序执行附件脚本，谢谢。<br/>" +
                        "        <br/>" +
                        "        <br/>" +
                        "        薛强  Qiang xue" +
                        "    </body>" +
                        "</html>")
                .attachmentName(new ArrayList<>() {{
                    this.add(targetFileName);
                    this.add("2-批量加扣.sql");
                    this.add("3-修改通道.sql");
                }})
                .build();

        if(UniversalTools.isWin()){
            ed.setAttachmentPath(targetBatchAddFeesWin);
        }else{
            ed.setAttachmentPath(targetBatchAddFees);
        }
        return sendEmailHandler.sendEMail(ed,true);
    }
}
