package com.pkufi.work.rest;

import com.pkufi.work.rest.enums.ReturnEnum;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @Description: TODO
 * @Author: qiang_xue
 * @date: 2023/3/8 15:30
 * @package: com.xue.working.vo
 * @Version: 1.0
 */
@Getter
@Setter
@AllArgsConstructor
@Builder(toBuilder = true)
public class Result<T> implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "返回状态码")
    private Integer code;
    @ApiModelProperty(value = "返回信息")
    private String message;
    @ApiModelProperty(value = "返回数据")
    private T data;

    public Result(Integer code, String message) {
        this.code = code;
        this.message = message;
    }
    public Result(T t) {
        this.data = t;
    }

    public static <T> Result<T> ok() {
        return new Result<>(ReturnEnum.SUCCESS.code(), ReturnEnum.SUCCESS.message(), null);
    }
    public static <T> Result<T> data(T data) {
        return new Result<>(ReturnEnum.SUCCESS.code(), ReturnEnum.SUCCESS.message(), data);
    }

    public static <T> Result<T> error() {
        return new Result<>(ReturnEnum.FAILURE.code(), ReturnEnum.FAILURE.message(), null);
    }
    public static <T> Result<T> error(String msg) {
        return new Result<>(ReturnEnum.FAILURE.code(), msg, null);
    }

    public static <T> Result<T> get(int code, String msg, T data) {
        return new Result<>(code, msg, data);
    }
}
