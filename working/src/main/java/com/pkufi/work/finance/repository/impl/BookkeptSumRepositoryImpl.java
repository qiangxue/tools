package com.pkufi.work.finance.repository.impl;

import com.pkufi.work.feecharge.entity.CommonRowResult;
import com.pkufi.work.finance.repository.BookkeptSumRepository;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@Repository
public class BookkeptSumRepositoryImpl implements BookkeptSumRepository {

private final JdbcTemplate primaryJdbcTemplate;

    private final JdbcTemplate secondaryJdbcTemplate;

    public BookkeptSumRepositoryImpl(@Qualifier("primaryJdbcTemplate") JdbcTemplate primaryJdbcTemplate, @Qualifier("secondaryJdbcTemplate") JdbcTemplate secondaryJdbcTemplate) {
        this.primaryJdbcTemplate = primaryJdbcTemplate;
        this.secondaryJdbcTemplate = secondaryJdbcTemplate;
    }

    public List<CommonRowResult> queryBookkeptSumData(List<String> params) {
    if (params == null || params.isEmpty()) {
        throw new IllegalArgumentException("Params list cannot be null or empty");
    }
    // Create the SQL query with placeholders for the parameters
    String sql = "SELECT ATTRIBUTE13 as SOURCE_BATCH_ID, SUM(a1.entered_dr) as sum_dr, SUM(a1.entered_cr) as sum_cr " +
                 "FROM apps.CUX_GL_INTERFACE_59 a1 " +
                 "WHERE a1.ATTRIBUTE13 IN (" + String.join(",", params.stream().map(p -> "?").toArray(String[]::new)) + ") " +
                 "AND EXISTS (SELECT 1 FROM gl_je_batches WHERE je_batch_id = a1.je_batch_id) " +
                 "GROUP BY ATTRIBUTE13";

    String[] paramsArray = params.toArray(new String[0]);
    return secondaryJdbcTemplate.query(sql, paramsArray, (rs, rowNum) -> {
        CommonRowResult commonRowResult = new CommonRowResult();
        int columnCount = rs.getMetaData().getColumnCount();
        for (int i = 1; i <= columnCount; i++) {
            String columnName = rs.getMetaData().getColumnName(i);
            Object value = rs.getObject(i);
            commonRowResult.put(columnName, value);
        }
        return commonRowResult;
    });
}

    @Override
    public List<CommonRowResult> queryBookkeptSumData(String params){

        String sql = "SELECT ATTRIBUTE13 as SOURCE_BATCH_ID,SUM(a1.entered_dr), SUM(a1.entered_cr) " +
                "FROM apps.CUX_GL_INTERFACE_59 a1 " +
                "WHERE a1.ATTRIBUTE13 in (-param-) " +
                "AND EXISTS (SELECT 1 FROM apps.gl_je_batches WHERE je_batch_id = a1.je_batch_id) " +
                "group by ATTRIBUTE13";

        sql = sql.replace("-param-", params);
        return secondaryJdbcTemplate.query(sql, new RowMapper<CommonRowResult>() {
            @Override
            public CommonRowResult mapRow(ResultSet rs, int rowNum) throws SQLException {
                CommonRowResult commonRowResult = new CommonRowResult();
                int columnCount = rs.getMetaData().getColumnCount();
                for (int i = 1; i <= columnCount; i++) {
                    String columnName = rs.getMetaData().getColumnName(i);
                    Object value = rs.getObject(i);
                    commonRowResult.put(columnName, value);
                }
                return commonRowResult;
            }
        });
    }
}
