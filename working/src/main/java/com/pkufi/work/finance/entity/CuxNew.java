package com.pkufi.work.finance.entity;

import lombok.Data;

import java.sql.Timestamp;

@Data
public class CuxNew {
    private String sourceBatchId;
    private String systemCode;
    private String status;
    private String importFlag;
    private Timestamp accountingDate;
    private Timestamp processDate;
    private String errorMessage;
    private String segment1;
    private String segment2;
    private String segment3;
    private String segment4;
    private String segment5;
    private String segment6;
    private String segment7;
    private String segment8;
    private String segment9;

}
