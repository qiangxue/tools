package com.pkufi.work.finance.task;

import com.pkufi.work.finance.service.EntryCheckService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalTime;

@Slf4j
@Component
@RequiredArgsConstructor
public class CheckTask {

    private final EntryCheckService capEntryCheckServiceImpl;

    //@Scheduled(fixedRate = 30*60*1000) // 每隔30分钟执行一次
    @Async  //异步事件支持
    @Scheduled(cron = "0 0 6-18/2 * * ?") // 每天6点到18点之间，每隔两小时执行一次
    public void entryCheckScheduledTask(){

        long startTime = System.currentTimeMillis();
        log.info("Task-"+startTime+" - EntryCheck starting...");
        capEntryCheckServiceImpl.execute();
        long endTime = System.currentTimeMillis();
        long duration = endTime - startTime;
        log.info("Task-"+startTime+" - EntryCheck execute completed in "+duration+" milliseconds");

    }

    public static boolean isOnTheHour() {
        LocalTime now = LocalTime.now();
        int minute = now.getMinute();
        return minute == 0;
    }
}
