package com.pkufi.work.finance.repository.impl;

import com.pkufi.work.finance.entity.CuxNew;
import com.pkufi.work.finance.repository.CuxNewRepository;
import com.pkufi.work.helper.DatabaseQueryHelper;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
@AllArgsConstructor
public class CuxNewRepositoryImpl implements CuxNewRepository {

    private final DatabaseQueryHelper databaseQueryHelper ;

    @Override
    public List<CuxNew> queryPendingStatus(){

        //查待入账数据,大于10条，邮件通知
        String sql = "SELECT " +
                "            source_batch_id, " +
                "            system_code, " +
                "            status " +
                "        FROM " +
                "            cux_59_gl_interface_new " +
                "        WHERE " +
                "            process_date > DATE '2023-04-25' " +
                "            AND import_flag = 'N' " +
                "        GROUP BY " +
                "            source_batch_id, " +
                "            system_code, " +
                "            status";


        return databaseQueryHelper.queryForEntities(sql, CuxNew.class);

    }

    @Override
    public List<CuxNew> queryErrorStatus(){

        String sql = "select distinct c.source_batch_id," +
                "                 c.system_code," +
                "                 c.status," +
                "                 c.import_flag " +
//                "                 c.accounting_date," +
//                "                 c.process_date" +
                "   from cux_59_gl_interface_new c" +
                "  where c.process_date > date '2023-04-25'" +
                "    and c.status = 'E'" +
                "    and c.import_flag = 'N'";

        return databaseQueryHelper.queryForEntities(sql, CuxNew.class);

    }

    @Override
    public List<CuxNew> queryErrorInfo(String str){
		//错误数据邮件通知（到处excel）
        String sql = "SELECT source_batch_id, error_message, segment1, segment2, segment3, segment4, segment5, segment6, segment7, segment8, segment9 from cux_59_gl_interface_new c where c.status= 'E' and c.error_message is not null and c.source_batch_id=? ";

        return databaseQueryHelper.queryForEntities(sql, CuxNew.class, str);

    }
}
