package com.pkufi.work.finance.service.impl;

import com.pkufi.work.finance.entity.CuxNew;
import com.pkufi.work.finance.service.CuxNewService;
import com.pkufi.work.finance.service.EntryCheckService;
import com.pkufi.work.mail.service.SendEmailHandler;
import com.pkufi.work.mail.vo.EmailData;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 个险CAP，资金ATS，费控HEC，全渠道APS，团险GRP入账检查
 */

@Service
@RequiredArgsConstructor
public class EntryCheckServiceImpl implements EntryCheckService {

    private final Logger log = LoggerFactory.getLogger(EntryCheckServiceImpl.class);

    private final CuxNewService cuxNewServiceImpl;
    private final SendEmailHandler sendEmailHandler;

    @Value("${fromMail.fromAddr}")
    private String fromAddr;
    @Override
    public void execute(){
        log.info("started execute cap-data check.");
        List<CuxNew> cnList = cuxNewServiceImpl.queryErrorStatus();
        if(cnList.isEmpty()) return;
            //System.exit(0);//0-程序正常终止
        //parallelStream并行处理可能会导致连接池的资源不足

        List<String> resultListAll = cnList.stream()
                .flatMap(c -> cuxNewServiceImpl.queryErrorInfo(c.getSourceBatchId()).stream()
                        .map(cuxNew -> cuxNew.getSourceBatchId() + "-" + cuxNew.getErrorMessage()))
                .collect(Collectors.toList());

//        int desiredParallelism = 4; // 设置期望的并行度为 4
//        ForkJoinPool forkJoinPool = new ForkJoinPool(desiredParallelism);
//        //forkJoinPool使用多线程并行调用是会出现线程1执行完一个查询后关闭连接，线程2继续执行是使用的连接损坏，不建议开启并行运行，或者调整代码设计
//        List<String> resultListAll = forkJoinPool.submit(() ->
//                cnList.parallelStream()
//                        .flatMap(c -> cuxNewServiceImpl.queryCheckError(c.getSourceBatchId()).stream()
//                                .map(cuxNew -> cuxNew.getSourceBatchId() + "-" + cuxNew.getErrorMessage()))
//                        .collect(Collectors.toList())
//        ).join();

        //错误批次-原因
        resultListAll.forEach(log::info);
        //发送邮件
        String emailContext = "<html>" +
                "    <head>" +
                "        <title></title>" +
                "    </head>" +
                "    <body>" +
                "        Dear <br/>" +
                "        &nbsp&nbsp&nbsp&nbsp。<br/>" +
                "        <br/>" +
                "        <br/>" +
                "        薛强  Qiang xue" +
                "    </body>" +
                "</html>";

        sendEmailHandler.sendEMail(EmailData.builder()
                .fromAddr(fromAddr)
                .toList(Collections.singletonList(fromAddr))
                .subject("CAP 入账检查")
                .context(emailContext)
                .build(),false);

    }

    //错误原因
}
