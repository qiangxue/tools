package com.pkufi.work.finance.service.impl;

import com.pkufi.work.finance.entity.CuxNew;
import com.pkufi.work.finance.repository.CuxNewRepository;
import com.pkufi.work.finance.service.CuxNewService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
@AllArgsConstructor
public class CuxNewServiceImpl implements CuxNewService {

    private final CuxNewRepository cuxNewRepository;

    @Override
    public List<CuxNew> queryPendingStatus(){
        return cuxNewRepository.queryPendingStatus();
    }

    @Override
    public List<CuxNew> queryErrorStatus() {
        return cuxNewRepository.queryErrorStatus();
    }

    @Override
    public List<CuxNew> queryErrorInfo(String str){
        return cuxNewRepository.queryErrorInfo(str);
    }


}
