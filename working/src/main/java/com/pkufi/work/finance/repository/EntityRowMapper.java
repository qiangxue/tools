package com.pkufi.work.finance.repository;

import com.pkufi.work.helper.DatabaseQueryHelper;
import com.pkufi.work.util.ConverterUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.RowMapper;

import java.lang.reflect.Field;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

public class EntityRowMapper<T> implements RowMapper<T> {

    private static final Logger LOGGER = LoggerFactory.getLogger(DatabaseQueryHelper.class);

    private final Class<T> entityClass;

    public EntityRowMapper(Class<T> entityClass) {
        this.entityClass = entityClass;
    }

    @Override
    public T mapRow(ResultSet resultSet, int rowNum) throws SQLException {

        try {

            T entity = entityClass.getDeclaredConstructor().newInstance();

            /*Field[] fields = entityClass.getDeclaredFields();
            for (Field field : fields) {
                field.setAccessible(true);
                String columnName = ConverterUtils.convertToUnderscore(field.getName());
                LOGGER.info("from "+columnName+" ==>");
                Object columnValue = resultSet.getObject(columnName);
                LOGGER.info("==> to"+columnName);
                field.set(entity, columnValue);
            }*/

            ResultSetMetaData metaData = resultSet.getMetaData();
            int columnCount = metaData.getColumnCount();
            Field field;
            for (int i = 1; i <= columnCount; i++) {
                String columnName = metaData.getColumnName(i);
                field = entityClass.getDeclaredField(ConverterUtils.convertToCamelCase(columnName));
                field.setAccessible(true);
                LOGGER.info("from " + columnName + " ==>");
                Object columnValue = resultSet.getObject(columnName);
                LOGGER.info("==> to" + columnName);
                field.set(entity, columnValue);

            }
            return entity;

        } catch (Exception e) {
            throw new SQLException("Failed to map rows to entities", e);
        }
    }

}