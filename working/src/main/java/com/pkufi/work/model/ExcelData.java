package com.pkufi.work.model;

import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@Builder
public class ExcelData {
    private List<String> headerRow;
    private List<List<String>> dataRows;
    @NotNull
    private CellStyleInfo headerCellStyle;
    @NotNull
    private CellStyleInfo dataCellStyle;

}
